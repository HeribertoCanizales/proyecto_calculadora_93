package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText txtNum1, txtNum2;
    private EditText txtResultado;
    private Button btnSuma,btnResta,btnMult,btnDivi,btnLimpiar,btnCerrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResultado = (EditText) findViewById(R.id.txtResultado);
        btnSuma = (Button) findViewById(R.id.btnSuma);
        btnResta = (Button) findViewById(R.id.btnResta);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnDivi = (Button) findViewById(R.id.btnDivi);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
    }
    public void setEventos(){

        this.btnSuma.setOnClickListener(this);
        this.btnResta.setOnClickListener(this);
        this.btnDivi.setOnClickListener(this);
        this.btnMult.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch( view.getId()){

            case  R.id.btnSuma :
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    Operaciones op= new Operaciones();
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText("" + op.suma());
                }
                break;
            case R.id.btnResta:
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    Operaciones op = new Operaciones();
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText("" + op.resta());
                }
                break;
            case R.id.btnMult:
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    Operaciones op = new Operaciones();
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText("" + op.mult());
                }
                break;
            case R.id.btnDivi:
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    Operaciones op = new Operaciones();
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText("" + op.div());
                }
                break;
            case R.id.btnLimpiar:
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    txtNum1.setText("");
                    txtNum2.setText("");
                    txtResultado.setText("");
                }
                break;
            case R.id.btnCerrar:
                finish();
                break;

        }

    }


    @Override
    public void onClick(View view) {

        switch( view.getId()){

            case  R.id.btnSuma :
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    Operaciones op= new Operaciones();
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText("" + op.suma());
                }
                break;
            case R.id.btnResta:
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    Operaciones op = new Operaciones();
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText("" + op.resta());
                }
                break;
            case R.id.btnMult:
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    Operaciones op = new Operaciones();
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText("" + op.mult());
                }
                break;
            case R.id.btnDivi:
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    Operaciones op = new Operaciones();
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResultado.setText("" + op.div());
                }
                break;
            case R.id.btnLimpiar:
                break;
            case R.id.btnCerrar:
                break;

        }

    }

}
